# Proyecto Integrado DAM

Ejemplo de e-commerce basado en arquitectura microservicios.

En él se utilizan las tecnologías:

- Servidor de aplicación: [Node.js](https://nodejs.org/es/)
- Framework Node.js: [Express.js](https://expressjs.com/)
- Motor de plantillas: [Handlebars.js](https://handlebarsjs.com/)
- Framework CSS: [Bootstrap](https://getbootstrap.com/)
- Pruebas: [Mocha.js](https://mochajs.org/), [Chai.js](https://www.chaijs.com/), [Cypress](https://www.cypress.io/)
- BBDD: [MongoDB](https://www.mongodb.com/es)
- ORM: [Mongoose.js](https://mongoosejs.com/)
- Despliegue: [Docker](https://www.docker.com/), [docker-compose](https://docs.docker.com/compose/)


## Scripts / comandos

- `$ ./clean.sh`: eliminar dependencias descargadas en todos los proyectos.
- `$ ./install.sh`: instala dependencias en todos los proyectos.
- `$ docker-compose up -d --build`: generá y levantan los servicios dockerizados.
- `$ docker-compose down`: finaliza la ejecución de los contenedores levantados y los elimina.