/// <reference types="Cypress" />

/*
  A solid test generally covers 3 phases:

    1. Set up the application state.
    2. Take an action.
    3. Make an assertion about the resulting application state.
*/

// Añade 20 productos
before(() => {
  const method = "POST";
  const url = Cypress.env("products_rest_api_uri");

  for (let i = 1; i <= 20; i++) {
    let body = { name: `product ${i}`, price: i };
    cy.request(method, url, body);
  }
});

// Elimina usuario creado al terminar tests
after(() => {
  cy.request("DELETE", `${Cypress.env("users_rest_api_uri")}/by-username`, {
    username: "user@random.email.es",
  });
});

describe("The Home page", () => {
  it("successfully loads", () => {
    cy.visit("/");
  });
  it("successfully loads products list", () => {
    cy.visit("/");
    cy.get("#products-list").should("exist");
  });
  it("successfully loads next product page", () => {
    cy.visit("/").get("#nextPage").click();
    cy.get("#products-list").should("exist");
  });
  it("successfully loads previous product page", () => {
    cy.visit("/").get("#nextPage").click().get("#previousPage").click();
    cy.get("#products-list").should("exist");
  });
});

describe("The nav bar links", () => {
  it("successfully redirects to home", () => {
    cy.visit("/login");
    cy.get(".nav-link").contains("Home").click();
    cy.get("#products-list").should("exist");
  });
  it("successfully redirects to login form", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Login").click();
    cy.get("#login-form").should("exist");
  });
  it("successfully redirects to sigunp form", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Signup").click();
    cy.get("#signup-form").should("exist");
  });
});

describe("The nav bar search", () => {
  it("successfully search products", () => {
    cy.visit("/").get("#tb-search").type("prod").get("#btn-search").click();
    cy.get("#products-list").should("exist");
  });
  it("successfully shows 'any products found' message", () => {
    cy.visit("/").get("#tb-search").get("#btn-search").click();
    cy.get("#any-prod-found-div").should("exist");
  });
  it("successfully shows 'invalid product name' message", () => {
    cy.visit("/").get("#tb-search").type("_").get("#btn-search").click();
    cy.get("#error-msg").should("exist");
  });
});

describe("The product details page", () => {
  it("succesffully loads product image and redirect to login button", () => {
    cy.visit("/");
    cy.get("#btn-buy-product").click();
    cy.get("#prod-img").should("exist");
    cy.get("#btn-redirect-login").should("exist");
  });
  it("successfully redirect to login form if user isn't logged", () => {
    cy.visit("/");
    cy.get("#btn-buy-product").click();
    cy.get("#btn-redirect-login").click();
    cy.get("#login-form").should("exist");
  });
});

describe("The signup form", () => {
  it("successfully redirects to login form", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Signup").click();
    cy.get("#redirect-to-login").click();
    cy.get("#login-form").should("exist");
  });
  it("successfully validate inputs", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Signup").click();
    cy.get("#email").type("email");
    cy.get("#password").type("password");
    cy.get("#signup-button").click();
    cy.url().should("contain", "/users/signup");
  });
  it("successfully signup a user", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Signup").click();
    cy.get("#email").type("user@random.email.es");
    cy.get("#password").type("passwordpassword");
    cy.get("#confirmPassword").type("passwordpassword");
    cy.get("#signup-button").click();
    cy.get("#section-selector").should("exist");
  });
  it("successfully checks user exists and can't be created again", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Signup").click();
    cy.get("#email").type("user@random.email.es");
    cy.get("#password").type("passwordpassword");
    cy.get("#confirmPassword").type("passwordpassword");
    cy.get("#signup-button").click();
    cy.get("#error-msg").should("exist");
  });
});

describe("The login form", () => {
  it("successfully redirects to signup form", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Login").click();
    cy.get("#redirect-to-signup").click();
    cy.get("#signup-form").should("exist");
  });
  it("successfully validate inputs", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Login").click();
    cy.get("#email").type("email");
    cy.get("#password").type("password");
    // Comprueba que despues introducir datos errores sigue en misma página
    cy.get("#login-button").click();
    cy.url().should("contain", "/users/login");
  });
  it("successfully checks user not exists", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Login").click();
    cy.get("#email").type(`nonExistingUser@email.com`);
    cy.get("#password").type("password");
    cy.get("#login-button").click();
    cy.get("#error-msg").should("exist");
  });
  it("successfully login", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Login").click();
    cy.get("#email").type(`user@random.email.es`);
    cy.get("#password").type("passwordpassword");
    cy.get("#login-button").click();
    cy.url().should("contains", "/users/dashboard");
  });
});

describe("The personal space", () => {
  it("successfully access profile section", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Login").click();
    cy.get("#email").type(`user@random.email.es`);
    cy.get("#password").type("passwordpassword");
    cy.get("#login-button").click();
    cy.get("#btn-profile").click();
    cy.get("#username").should("exist");
    cy.get("#usernumber").should("exist");
    cy.get("#phone").should("exist");
  });
  it("successfully access profile order", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Login").click();
    cy.get("#email").type(`user@random.email.es`);
    cy.get("#password").type("passwordpassword");
    cy.get("#login-button").click();
    cy.get("#btn-profile").click();
    cy.get("#username").should("exist");
    cy.get("#usernumber").should("exist");
    cy.get("#phone").should("exist");
  });
  it("successfully redirects to personal space", () => {
    cy.visit("/");
    cy.get(".nav-link").contains("Login").click();
    cy.get("#email").type(`user@random.email.es`);
    cy.get("#password").type("passwordpassword");
    cy.get("#login-button").click();
    cy.get(".nav-link").contains("Dashboard").click();
    cy.url().should("contain", "/users/dashboard");
  });
});
