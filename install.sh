#! /bin/bash

# Instala dependencias de todos los proyectos

cd ./e2e_tests
npm install

cd ../orders_rest-api
npm install

cd ../products_rest-api
npm install

cd ../users_rest-api
npm install

cd ../web
npm install