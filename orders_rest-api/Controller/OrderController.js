"use strict";

const { param, body, validationResult } = require("express-validator");
// const logger = require("../Config/logger");
const OrderModel = require("../Models/OrderModel");
const createError = require("http-errors");
const paypal = require("@paypal/checkout-server-sdk");
const { buildCustomRequestBody, filtered_keys } = require("../Utils/Utils");
const crypto = require("crypto");
const logger = require('../Config/logger')
//
// Creating an environment
const clientId = process.env.PAYPAL_CLIENT_ID;
const clientSecret = process.env.PAYPAL_CLIENT_SECRET;
// This sample uses SandboxEnvironment. In production, use LiveEnvironment
const environment = new paypal.core.SandboxEnvironment(clientId, clientSecret);
const client = new paypal.core.PayPalHttpClient(environment);
//

require("dotenv").config();

module.exports = {
  validate(method) {
    switch (method) {
      case "captureOrder": {
        return [
          body("userid")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("user-id is required")
            .isMongoId()
            .withMessage("user id not valid"),
          body("prodId")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("product ID is required")
            .isMongoId()
            .withMessage("product id not valid"),
          body("orderID")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("order ID is required"),
        ];
      }
      case "getUserOrders": {
        return [
          param("userId")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("User ID is required")
            .isMongoId()
            .withMessage("User ID invalid"),
        ];
      }
      case "captureRefund": {
        return [
          body("orderId")
            .exists({ checkFalsy: true, checkNull: true })
            .withMessage("Order ID is required")
            .isInt({ min: 0 })
            .withMessage("Order ID is invalid"),
        ];
      }
    }
  },
  /**
   * Crea pedido
   */
  async captureOrder(req, res, next) {
    try {
      // res.json({ msg: "order created" });
      //   const errors = validationResult(req).array({ onlyFirstError: true });

      //   if (errors.length !== 0) {
      //     logger.info(
      //       "POST /api/orders/create OrdersController.create()" +
      //         errors.map((x) => JSON.stringify(x, null, 2))
      //     );
      //     res.status(422).json({ errors: errors });
      //   }

      // 2a. Get the order ID from the request body
      const orderID = req.body.orderID;

      // 3. Call PayPal to capture the order
      const request = new paypal.orders.OrdersCaptureRequest(orderID);
      request.requestBody({});

      // Ejecuta petición
      const capture = await client.execute(request);

      /*
            Petición API empresa logística para obtener coste, tiempo estimado, etc.
          */
      const shipmentCost = 10;
      // Genera hash tipo sha1 de un número aleatorio
      const shipmentId = crypto
        .createHmac("sha1", "s3cR37")
        .update(Math.random() + "")
        .digest("hex");

      // Extrae ID del pago
      const captureID =
        capture.result.purchase_units[0].payments.captures[0].id;

      // Cuenta número pedidos para crear ID auto-númerico
      const reference_id = (await OrderModel.countDocuments()) + 1;

      // Extrae nombre y apellidos persona realizo el pago
      const { given_name, surname } = capture.result.payer.name;

      const { address } = capture.result.purchase_units[0].shipping;
      // Extrae claves del objeto address según una expresión regular
      const address_line = filtered_keys(address, /^address_line/);
      const admin_area = filtered_keys(address, /^admin_area/);
      const country_code = address.country_code;
      const postal_code = address.postal_code;

      // Necesita parse datos extraidos objeto
      const amount = Number.parseFloat(
        capture.result.purchase_units[0].payments.captures[0].amount.value
      );
      const total_amount = Math.round(amount + shipmentCost);
      // console.log(`${amount} + ${shipmentCost} = ${total_amount}`);

      // Crea pedido en bbbdd
      const createdOrder = await OrderModel.create({
        order_number: reference_id,
        user_id: req.body.userId,
        status: "created",
        external_payment_id: captureID,
        products: {
          product_id: req.body.prodId,
          price: 44.77,
          quantity: 1,
        },
        shipment: {
          shipment_id: shipmentId,
          method: "UPS standart",
          cost: shipmentCost,
          full_name: `${given_name} ${surname}`,
          delivery_address: {
            address_line: address_line,
            admin_area: admin_area,
            postal_code: postal_code,
            country_code: country_code,
          },
        },
        total_amount: total_amount,
      });

      // Si todo correcto devuel pedido creado
      res.status(200).json(createdOrder);
      //
    } catch (err) {
      // console.log(err);
      logger.error(
        "POST /api/orders OrderController.captureOrder" + err
      );
      next(createError(500, err));
    }
  },
  /**
   * Cambia estado pedido a 'refunded' (devuelto/reembolsado)
   */
  async captureRefund(req, res, next) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        logger.info(
          "POST /api/orders/refund OrdersController.captureRefund()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(422).json({ errors: errors });
      }

      // res.json(req.body.orderId);
      const orderId = req.body.orderId;

      //  Obtiene ID asignado por paypal filtrando por número de pedido
      const { external_payment_id } = await OrderModel.findOne(
        { order_number: orderId },
        "external_payment_id"
      ).exec();
      console.log(external_payment_id);
      // Si external_payment_id falsy, pedido no encontrado
      // const external_payment_id = false;
      if (!external_payment_id) {
        res.status(404);
      }

      // Realiza petición de reembolso a Paypal
      const request = new paypal.payments.CapturesRefundRequest(
        external_payment_id
      );
      request.requestBody({});

      // Ejecuta petición y actualiza estado pedido en BBDD
      const refund = await client.execute(request);
      // console.log(refund);
      const updatedOrder = await OrderModel.findOneAndUpdate(
        { order_number: orderId },
        { status: "refunded" },
        { new: true }
      );
      // Devuelve repuesta con estado 200 y pedido ctualizado
      res.status(200).json(updatedOrder);
      //
    } catch (err) {
      // console.log(err);
      logger.error(
        "PATCH /api/orders/refund OrderController.captureRefund" + err
      );
      next(createError(500, err));
    }
  },
  /**
   * Obtiene pedidos un usuario
   */
  async getUserOrders(req, res, next) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        logger.info(
          "GET /api/orders/userid/:userId OrdersController.getUserOrders()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(422).json({ errors: errors });
      }

      const userId = req.params.userId;

      // Busca y devuelve documentos con campo user_id igual a userId
      const orders = await OrderModel.find({ user_id: userId });
      if (orders) {
        res.status(201).json(orders);
      } else {
        res.status(404).end();
      }
    } catch (error) {
      // console.log(error);
      logger.error(
        "GET /api/orders/:userId OrderController.getUserOrders" + err
      );
      next(createError(500));
    }
  },
};
