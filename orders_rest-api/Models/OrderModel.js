"use strict";

/* Imports */
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const getIncrementNum = async () => {
  const totalDocs = OrderSchema.countDocuments();
  return totalDocs + 1;
};

// {
//   "address": "<String>",
//   "item": {
//       "itemId": 1,
//       "name": "<String>",
//       "price": 2000,
//       "description": "<String>"
//   }
// }

const AddressSchema = new Schema({
   // calle, número, planta, ...
  address_line: { type: [String] },
   // ciudad, localidad, estado, comunidad autonoma, ...
  admin_area: { type: [String] },
  // Código postal
  postal_code: { type: Number },
  // Código de país
  country_code: { type: String, match: /[A-Z]{2}/ }, // US, ES
});

const ProductsSchema = new Schema(
  {
    // ID
    product_id: { type: mongoose.ObjectId, required: true },
    // Precio
    price: { type: Number, min: 1, max: 999, required: true },
    // Cantidad
    quantity: { type: Number, min: 1, max: 999, required: true },
  },
  { _id: false }
);

const ShipmentSchema = new Schema(
  {
    // id envío generado por transportista
    shipment_id: { type: String, required: true, unique: true },
    // Tipo de envío
    method: { type: String, required: true },
    // Coste envío
    cost: { type: Number, min: 1, max: 999 },
    // Nombre completo destinatario
    full_name: { type: String, required: true },
    // Dirección envío
    delivery_address: { type: AddressSchema, required: true },
  },
  { _id: false }
);

// Conjunto estados permitidos
const statusEnum = [
  "created",
  "awaiting pickup",
  "shipped",
  "completed",
  "refunded",
];

const OrderSchema = new Schema(
  {
    // Número de pedido
    order_number: { type: Number, min: 1, required: true, unique: true },
    user_id: { type: mongoose.ObjectId, required: true },
    // Estado actual pedido
    status: { type: String, enum: statusEnum, required: true },
    // id generado en paypal, etc.
    external_payment_id: { type: String, required: true },
    // Listado de productos
    products: { type: [ProductsSchema] },
    // Información del envío
    shipment: { type: ShipmentSchema },
    // Coste total del pedido
    total_amount: { type: Number, min: 1, max: 999, required: true },
  },
  {
    // crea campos createdAt y updatedAt
    timestamps: true,
  }
);

// OrderSchema.pre("save", function (next) {
//   // this.order_number = OrderSchema.countDocuments() + 1
//   //
//   // console.log("Executing PRE SAVE hook");
//   const productsAmount = this.products.reduce(
//     (total, obj) => total + obj.price,
//     0
//   );
//   this.amount = this.productsAmount + this.shipment.cost;
//   next();
// });

module.exports = mongoose.model("orders", OrderSchema);
