"use strict";

const express = require("express");
const router = express.Router();
const createError = require("http-errors");
const OrderController = require("../Controller/OrderController");

router
  /**
   * GET /api/orders
   */
  // .get("", (req, res, next) => {
  //   console.log("hello from GET /");
  //   if (req.query.userid) res.json({ msg: `GET ?userid=${req.query.userid}` });
  //   next(createError(404));
  // })
  /**
   * GET /api/orders/userid/:userId Obtiene listas pedidos usuario
   * con ID igual al parámetro :userId
   */
  .get(
    "/userid/:userId",
    OrderController.validate("getUserOrders"),
    OrderController.getUserOrders
  )
  /**
   * POST /orders Crea un pedido
   */
  .post(
    "",
    OrderController.validate("captureOrder"),
    OrderController.captureOrder
  )
  /**
   * PATCH /orders/refund Realiza devolución un pedido
   */
  .patch(
    "/refund",
    OrderController.validate("captureRefund"),
    OrderController.captureRefund
  );
// // Actualiza un pago
// .put("/:id", (req, res) => {
//   res.json({ msg: `payment with id ${req.params.id} updated` });
// })
// // Elimina un pago
// .delete("", (req, res) => {
//   res.json({ msg: `payment with id ${req.params.id} deleted.` });
// });

module.exports = router;
