"use strict";

const express = require("express");
const router = express.Router();
const logger = require("../Config/logger");

//Obtiene una lista de todos los productos
router.get("/", (req, res) => {
  logger.info("Checked server health");
  res.sendStatus(200);
});

module.exports = router;
