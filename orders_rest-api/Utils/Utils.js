"use strict";

require("dotenv").config();

const return_url = process.env.ORDER_RETURN_URL;
const cancel_url = process.env.ORDER_CANCEL_URL;
const brand_name = process.env.ORDER_BRAND_NAME;
const locale = process.env.ORDER_LOCALE;

/**
 * Setting up the complete JSON request body for creating the Order. The Intent
 * in the request body should be set as "AUTHORIZE" for Authorize intent flow
 * or "CAPTURE" for Capture intent flow.
 *
 * @param {string} intent Tipo operación intentar a realizar
 * @param {number} reference_id Numero id pedido
 * @param {number} total_amount Montante total
 * @param {string} shipping_method Transportista / método de envío
 * @param {string} full_name Nombre completo destinatario
 * @param {object} address Dirección donde realizar envío
 */
function buildCustomRequestBody(
  intent,
  reference_id,
  total_amount,
  shipping_method,
  full_name,
  address
) {
  return {
    intent: intent,
    application_context: {
      return_url: return_url,
      cancel_url: cancel_url,
      brand_name: brand_name,
      locale: locale || "es-ES",
      user_action: "commit",
    },
    purchase_units: [
      {
        reference_id: reference_id,
        description: "_description",

        custom_id: "custom_id",
        soft_descriptor: "soft_description",
        amount: {
          currency_code: "EUR",
          value: total_amount,
        },
        shipping: {
          method: shipping_method,
          name: {
            full_name: full_name,
          },
          address: address,
          // "address": {
          //     "address_line_1": "123 Townsend St",
          //     "address_line_2": "Floor 6",
          //     "admin_area_2": "San Francisco",
          //     "admin_area_1": "CA",
          //     "postal_code": "94107",
          //     "country_code": "US"
          // }
        },
      },
    ],
  };
}

/**
 * 
 * @param {Object} obj Objeto con propiedades a filtrar
 * @param {Regex} filter Expresión regular a usar de filtro
 */
function filtered_keys(obj, filter) {
  let key, keys = [];
  for (key in obj)
    if (obj.hasOwnProperty(key) && filter.test(key)) keys.push(key);
  return keys;
}

module.exports = {
  buildCustomRequestBody,
  filtered_keys
};
