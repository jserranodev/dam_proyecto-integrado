const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
// const logger = require("morgan");
const morgan = require("morgan"); // Logger estilo Apache Web Server
const logger = require("./Config/logger"); // Logger para resto operaciones
const createError = require("http-errors");
const Db = require("./Config/initDB");
const app = express();
const mongoose = require("mongoose");
require("dotenv").config();

/* DB */
Db.connect();
function intervalFunc() {
  // console.log(mongoose.connection.readyState);
  if (mongoose.connection.readyState == 0) {
    // console.log("Trying to reconnect to DB");
    logger.error("Trying to reconnect to DB");
    Db.connect();
  }
}
// Ejecuta la función anterior cada 5 segundo (5000 milisegundos)
const secondToCheck = process.env.DB_HEALTH_CHECK_RATE;
// Node.js: Timers
if (process.env.NODE_ENV !== "test")
  setInterval(intervalFunc, secondToCheck * 1000);
/* End DB */

/* Express middlewares */
// app.use(logger("dev"));
app.use(morgan("combined", { stream: logger.stream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/api/orders", require("./Routes/OrdersRouter"));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// errorHandler
app.use(function (err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  }
  // usa código estado de error o 500
  res.status(err.status || 500).json({ error: err });
});

module.exports = app;
