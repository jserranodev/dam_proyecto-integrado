"use strict";

const mongoose = require("mongoose");
const logger = require("./logger");

// /* DB */
// mongoose
//   .connect(process.env.MONGODB_URI, {
//     // user: process.env.DB_USER,
//     // pass: process.env.DB_PASS,
//     useFindAndModify: false, //https://mongoosejs.com/docs/deprecations.html#findandmodify
//     useUnifiedTopology: true, // Usa MongoDB driver 3.3.x
//     useNewUrlParser: true, // habilita nuevo parser
//   })
//   .then(() => {
//     if (NODE_ENV !== "test") console.log("DB Connected!");
//   })
//   .catch((err) => {
//     if (NODE_ENV !== "test") console.log(`DB Connection Error: ${err.message}`);
//   });

// let db = mongoose.connection;

// db.on("error", console.error.bind(console, "connection error:"));

// module.exports = db;

module.exports = {
  connect() {
    mongoose
      .connect(process.env.MONGODB_URI, {
        dbName: process.env.DB_NAME,
        // user: process.env.DB_USER,
        // pass: process.env.DB_PASS,
        useFindAndModify: false, //https://mongoosejs.com/docs/deprecations.html#findandmodify
        useUnifiedTopology: true, // Usa MongoDB driver 3.3.x
        useNewUrlParser: true, // habilita nuevo parser
      })
      .then(() => {
        // console.log("DB Connected!");
        logger.info("BD connected");
      })
      // Se ejecuta trás él último intento de conexión
      .catch((err) => {
        logger.info(`DB Connection Error: ${err.message}`);
        // console.log(`DB Connection Error: ${err.message}`);
        // // Vuelve a ejecutar este método hasta que se establezca
        // // conexión.
        // this.connect();
      });
  },
  getConnection() {
    this.currConn = mongoose.connection;
  },

  // setEvents() {
  //   this.getConnection;
  //   this.currConn.on("connecting", console.log("Connecting..."));
  //   this.currConn.on("connected", console.log("Connected"));
  //   this.currConn.on("disconnected", console.log("Disconnected"));
  //   this.currConn.on("reconnectFailed", () => {
  //     console.log("Run out of reconectTries");
  //     this.connect();
  //   });
  // },
};
