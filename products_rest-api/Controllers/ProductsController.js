"use strict";

const ProductsModel = require("../Models/ProductsModel");
const { param, body, validationResult } = require("express-validator");
var logger = require("../Config/logger");
const { query } = require("../Config/logger");

module.exports = {
  validate(method) {
    switch (method) {
      case "getPaginatedProducts": {
        return [
          body("page")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("Page number is required")
            .isInt({ min: 1 })
            .withMessage("Page number not valid"),
          body("limit")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("Product limit number is required")
            .isInt({ min: 1 })
            .withMessage("Product limit number not valid"),
        ];
      }
      case "getProductById": {
        return [
          param("productId")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("ID is required")
            .isMongoId()
            .withMessage("ID not valid"),
        ];
      }
      case "getProductsByName": {
        return [
          // body("prodName")
          param("name")
            .trim()
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("Name is required  and can't be falsy")
            .custom((name) => {
              // regex: letras, números y espacios en blanco
              const result = /^[a-zA-Z0-9 ]*$/g.test(name);
              if (result) return Promise.resolve();
              return Promise.reject(
                "Name must contains only alphanumeric and whitespace charactes"
              );
            }),
        ];
      }
      case "createProduct": {
        return [
          // .exists() === required === NOT NULL
          body("name")
            .trim()
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("Name is required  and can't be falsy")
            .custom((name) => {
              // regex: letras, números y espacios en blanco
              const result = /^[a-zA-Z0-9 ]*$/g.test(name);
              if (result) return Promise.resolve();
              return Promise.reject(
                "Name must contains only alphanumeric and whitespace charactes"
              );
            })
            // .isAlphanumeric()
            // .withMessage("Name must be alphanumeric")
            .isLength({ min: 5, max: 50 })
            .withMessage("Name must have a length between 5 and 50 charactes"),
          body("price")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("Price is required and can't be falsy")
            .isFloat({ min: 1, max: 999 })
            .withMessage("Price must be a float between 1 and 999"),
        ];
      }
      case "updateProduct": {
        return [
          param("productId")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("ID is required")
            .isMongoId()
            .withMessage("ID not valid"),
          body("name")
            .optional({ nullable: true })
            .trim()
            .custom((name) => {
              // regex: letras, números y espacios en blanco
              const result = /^[a-zA-Z0-9 ]*$/g.test(name);
              if (result) return Promise.resolve();
              return Promise.reject(
                "Name must contains only alphanumeric and whitespace charactes"
              );
            })
            // .isAlphanumeric()
            // .withMessage("Name must be alphanumeric")
            .isLength({ min: 5, max: 50 })
            .withMessage("Name must have a length between 5 and 50 characters"),
          body("price", "Price must be a float between 1 and 999")
            .optional({ nullable: true })
            .isFloat({ min: 1, max: 999 }),
        ];
      }
      case "removeProduct": {
        return [
          param("productId")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("ID is required")
            .isMongoId()
            .withMessage("ID not valid"),
        ];
      }
    }
  },
  getPaginatedProducts(req, res) {
    try {
      console.log(req.body);

      // Finds the validation errors in this request and wraps them in an object with handy functions
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        //422 Unprocessable Entity
        logger.info(
          "GET /api/products/ ProductController.getPaginatedProducts()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(404).json({ errors: errors });
        return;
      }

      let page = parseInt(req.body.page);
      const limit = parseInt(req.body.limit);
      let startIndex = (page - 1) * limit;
      const endIndex = page * limit;
      const result = {};
      ProductsModel.countDocuments({}, (err, count) => {
        // console.log(count);

        // console.log(`${startIndex} > (${limit} - (${count})`);
        // if (startIndex > count - limit ) {
        //   startIndex = count - limit;
        // }
        result.previousPage = page - 1;
        result.currPage = page;
        if (endIndex < count) result.nextPage = page + 1;
        result.productLimit = limit;

        // console.log("\n", "----", result,startIndex, "----","\n");

        ProductsModel.find()
          .limit(limit)
          .skip(startIndex)
          .then((docs) => {
            // console.log(docs);
            console.log(result);
            result.results = docs;
            res.json(result);
            // res.sendStatus(200);
          });
      });
    } catch (error) {
      logger.error(
        "GET /api/products/ ProductController.getPaginatedProducts()" + err
      );
      res.status(500);
    }
  },
  getProductById(req, res) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        logger.info(
          "GET /api/products/ID ProductController.getProductById()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(422).json({ errors: errors });
        return;
      }

      ProductsModel.findById(req.params.productId)
        .then((data) => {
          if (data) res.json(data);
          else res.sendStatus(404);
        })
        .catch((err) => res.sendStatus(404));
    } catch (err) {
      logger.error(
        "GET /api/products/ID ProductController.getProductById()" + err
      );
      res.sendStatus(500);
    }
  },
  async getProductsByName(req, res) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        logger.info(
          "GET /api/products/ID ProductController.getProductsByName()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(422).json({ errors: errors });
        return;
      }

      const name = req.params.name;
      console.log(`name: ${name}`);
      const foundProd = await ProductsModel.find({
        name: new RegExp(`.*${name}.*`, "i"),
      });
      if (foundProd) {
        res.status(200).json(foundProd);
      } else {
        res.sendStatus(404);
      }
      //
    } catch (err) {
      logger.error(
        "GET /api/products/ID ProductController.getProductById()" + err
      );
      res.sendStatus(500);
    }
  },
  createProduct(req, res, next) {
    try {
      // Finds the validation errors in this request and wraps them in an object with handy functions
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        //422 Unprocessable Entity
        logger.info(
          "POST /api/products/ ProductController.createProduct()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(422).json({ errors: errors });
        return;
      }

      const { name, price } = req.body;

      const productToCreate = new ProductsModel({
        name: name,
        price: parseFloat(price).toFixed(2),
      });

      productToCreate.save().then((createdProduct) => {
        if (createdProduct) res.json(createdProduct);
        else res.sendStatus(500);
      });
    } catch (err) {
      logger.error(
        "POST /api/products/ID ProductController.getProductById()" + err
      );
      res.sendStatus(500);
    }
  },
  //
  removeProduct(req, res) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        logger.info(
          "POST /api/products/ ProductController.removeProduct()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(400).json({ errors: errors });
        return;
      }
      ProductsModel.findOneAndRemove({ _id: req.params.productId })
        .then((removedProduct) => {
          if (removedProduct) res.json(removedProduct);
          else res.sendStatus(404);
        })
        .catch((err) => console.log(err));
    } catch (err) {
      logger.error(
        "DELETE /api/products/ID ProductController.removeProduct()" + err
      );
      res.sendStatus(500);
    }
  },
  //
  updateProduct(req, res) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        //422 Unprocessable Entity
        logger.info(
          "PUT /api/products/ID ProductController.updateProduct()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(422).json({ errors: errors });
        return;
      }
      const filter = req.params.productId;
      // Redondea precio a dos decimales
      const fixedPrice = req.body.price
        ? parseFloat(req.body.price).toFixed(2)
        : null;

      ProductsModel.findById(filter)
        .then((product) => {
          // Utilizamos datos anteriores si no son aportados en
          // petición.
          // const update = {
          //   name: req.body.name || product.name,
          //   price: fixedPrice || product.price,
          // };

          const productToUpdate = product;

          productToUpdate.name = req.body.name || product.name;
          productToUpdate.price = fixedPrice || product.price;

          // console.log("filer:" + filter);

          // ProductsModel.findOneAndUpdate(
          //   // { _id:  },
          //   // { price: fixedPrice },
          //   filter,
          //   update,
          //   { returnOriginal: false } // hace devuelva documento modificado
          // ).then((updatedProduct) => {
          //   // Si encuentra producto updatedProduct !== null entonces
          //   //   devuelve producto actualizado.
          //   // Si updatedProduct === null entonces NOT FOUND
          //   console.log("updatedProduct id: " + updatedProduct._id);
          //   if (updatedProduct) res.json(updatedProduct);
          //   else res.sendStatus(500);
          // });

          // Se recomienda usar save() en documentación al actualizar
          // documentos.
          productToUpdate.save().then((updatedProduct) => {
            if (updatedProduct) res.json(updatedProduct);
            else res.sendStatus(500);
          });
        })
        // Captura excepción en findById y findOneAndUpdate
        .catch((err) => {
          logger.info(
            "PUT /api/products/ID ProductController.updateProduct()" + err
          );
          res.sendStatus(400);
        });
    } catch (err) {
      logger.error(
        "PUT /api/products/ID ProductController.updateProduct()" + err
      );
      res.sendStatus(500);
    }
  },
};
