"use strict";

const mongoose = require("mongoose");

const ProductSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    min: 1,
    max: 999,
    required: true,
  },
});

module.exports = mongoose.model("products", ProductSchema);
