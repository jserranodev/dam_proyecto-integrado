"use strict";

const express = require("express");
const router = express.Router();

// const ProductsModel = require("../Models/ProductsModel");
const ProductController = require("../Controllers/ProductsController");

router
  /**
   * GET /api/products Obtiene listado productos páginados
   */
  .get(
    "/",
    ProductController.validate("getPaginatedProducts"),
    ProductController.getPaginatedProducts
  )
  /**
   * GET /api/products/search-by-name/:name Obtiene productos que
   * contengan el valor del parámetro :name en el nombre
   */
  .get(
    "/search-by-name/:name",
    ProductController.validate("getProductsByName"),
    ProductController.getProductsByName
  )
  /**
   * GET /api/products/:productId Obtiene producto con ID igual al
   * parámetro :productID
   */
  .get(
    "/:productId",
    ProductController.validate("getProductById"),
    ProductController.getProductById
  )
  /**
   * POST /api/products Crea un producto
   */
  .post(
    "/",
    ProductController.validate("createProduct"),
    ProductController.createProduct
  )
  /**
   * DELETE /api/products/:productId Elimina el producto con ID
   * igual al parámetro :productId
   */
  .delete(
    "/:productId",
    ProductController.validate("removeProduct"),
    ProductController.removeProduct
  )
  /**
   * PUT /api/products/:productsId Actualiza produto con ID igual
   * al parámetro :productId
   */
  .put(
    "/:productId",
    ProductController.validate("updateProduct"),
    ProductController.updateProduct
  );

module.exports = router;
