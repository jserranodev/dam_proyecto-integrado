"use strict";

const mongoose = require("mongoose");
// const request = require("supertest");
const chai = require("chai"),
  chaiHttp = require("chai-http");
const expect = chai.expect;
chai.use(chaiHttp);

const ProductModel = require("../Models/ProductsModel");
const mock_data = require("../mock_data/data.json");

/**
 * Conjunto de funciones que testean el el router, controlador y
 * modelo de productos.
 */
describe("Test products", function () {
  let app = undefined;

  /**
   * Inicia el servidor antes realizar tests
   */
  before((done) => {
    app = require("../index");
    done();
    // ProductModel.insertMany(mock_data, function (error, docs) {
    //   done();
    // });
  });

  /**
   * Antes cada test inserta documentos ejemplo
   */
  beforeEach((done) => {
    ProductModel.insertMany(mock_data, function (error, docs) {
      done();
    });
  });

  /**
   * Después cada test elimina todos los documentos
   */
  afterEach((done) => {
    ProductModel.remove();
    done();
  });

  /**
   * Trás finalizar los tests, elimina los datos utilizados, cierra
   * la conexión con la base de datos y cierra el servidor.
   */
  after((done) => {
    mongoose.connection.db.dropCollection("products").then(() => {
      mongoose.connection.close(() => {
        app.close(() => done());
      });
    });
  });

  /**
   * Testea ProductController.getAllProducts()
   */
  // describe("GET /api/products", () => {
  //   /**
  //    * Testea que /api/productos devuelva todos los documentos
  //    */
  //   it("/api/products/ responde con 200 y un array no vacío.", function (done) {
  //     chai
  //       .request(app)
  //       .get("/api/products")
  //       .end(function (err, res) {
  //         // console.log(res.body);
  //         expect(res).to.have.status(200);
  //         expect(res.body).to.be.an("array").that.is.not.empty;
  //         done();
  //       });
  //   });

  /**
   * Testea ProductController.getPaginatedProducts()
   */
  describe("GET /api/products", () => {
    /**
     * Testea que /api/productos devuelva todos los documentos
     */
    it("/api/products/ responde con 200 y un array con 2 resultados.", function (done) {
      chai
        .request(app)
        .get("/api/products")
        .set("Content-Type", "application/json")
        .send({ page: 1, limit: 2 })
        .end(function (err, res) {
          console.log(res.body);
          expect(res).to.have.status(200);
          expect(res.body.results).to.be.an("array").with.length(2);
          done();
        });
    });
  });

  /**
   * Testea ProductController.getProductById()
   */
  describe("GET /api/products/:id", function () {
    /**
     * Testea que /api/products/${id_existente} devuelva el documento
     * existente.
     */
    it("/api/products/${idProd1} responde con 200 y producto 'product 1'.", (done) => {
      // Consultado un producto para obtener su id.
      ProductModel.find({ name: "product 1" }, function (err, docs) {
        const idToSearch = docs[0]._id;
        // console.log("|||||||||||||||" + idToSearch);
        chai
          .request(app)
          .get(`/api/products/${idToSearch}`)
          .end(function (err, res) {
            // console.log(JSON.stringify(res.body, null, 2));
            expect(res).to.have.status(200);
            // expect(res).to.have.header("Content-Type", "application/json");
            expect(res.body._id).to.be.equal(idToSearch.toString());
            expect(res.body.name).to.be.equal("product 1");
            expect(res.body.price).to.be.equal(48.78);
            done();
          });
      });
    });
    /**
     * Testea que si se utiliza otra cosa que no sea un id de MongoDB
     * devuelva id invalido.
     */
    it("/api/products/loquesea responde con 400 e id invalido.", (done) => {
      chai
        .request(app)
        .get(`/api/products/loquesea`)
        .end(function (err, res) {
          // console.log(JSON.stringify(res.body, null, 2));
          expect(res).to.have.status(422);

          const error = res.body.errors[0];

          expect(error.value).to.be.equal("loquesea");
          expect(error.msg).to.be.equal("ID not valid");
          done();
        });
    });
    /**
     * Testea que devuelva 404 si no encuentra el documento buscado.
     */
    it("/api/products/5fc23e76f77e69262cbfecea responde con 404.", (done) => {
      chai
        .request(app)
        .get(`/api/products/5fc23e76f77e69262cbfecea`)
        .end(function (err, res) {
          expect(res).to.have.status(404);
          expect(res.text).to.be.equal("Not Found");
          done();
        });
    });
  });

  /**
   * Testea ProductController.createProduct()
   */
  describe("POST /api/products", () => {
    /**
     * Testea que no se crea documento por nombre no válido
     */
    it("{name: 'producto_5', price: 55} responde con 422 y 'Name must contains only alphanumeric and whitespace charactes'", function (done) {
      chai
        .request(app)
        .post("/api/products")
        .set("Content-Type", "application/json")
        .send({ name: "producto_5", price: 55 })
        .end(function (err, res) {
          expect(res).to.have.status(422);

          const error = res.body.errors[0];

          expect(error.msg).to.be.equal(
            "Name must contains only alphanumeric and whitespace charactes"
          );
          done();
        });
    });
    /**
     * Testea que no se crea documento por nombre no válido
     */
    it("{name: 'prod', price: 55} responde con 422 y 'Name must have a length between 5 and 50 charactes'", function (done) {
      chai
        .request(app)
        .post("/api/products")
        .set("Content-Type", "application/json")
        .send({ name: "prod", price: 55 })
        .end(function (err, res) {
          expect(res).to.have.status(422);

          const error = res.body.errors[0];

          expect(error.msg).to.be.equal(
            "Name must have a length between 5 and 50 charactes"
          );
          done();
        });
    });
    /**
     * Testea que no se crea documento por nombre no válido
     */
    it("{name: 'produuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuct', price: 55} responde con 422 y 'Name must have a length between 5 and 50 charactes'", function (done) {
      chai
        .request(app)
        .post("/api/products")
        .set("Content-Type", "application/json")
        .send({
          name: "produuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuct",
          price: 55,
        })
        .end(function (err, res) {
          expect(res).to.have.status(422);

          const error = res.body.errors[0];

          expect(error.msg).to.be.equal(
            "Name must have a length between 5 and 50 charactes"
          );
          done();
        });
    });
    /**
     * Testea que no se crea documento por falta de propiedades
     */
    it("{name: 'producto 5'} responde con 422 y 'Price is required and can't be falsy'", function (done) {
      chai
        .request(app)
        .post("/api/products")
        .set("Content-Type", "application/json")
        .send({ name: "product 5" })
        .end(function (err, res) {
          expect(res).to.have.status(422);

          const error = res.body.errors[0];

          expect(error.msg).to.be.equal("Price is required and can't be falsy");
          done();
        });
    });
    /**
     * Testea que no se crea documento por precio no valido
     */
    it("{name: 'producto 5', price: -1} responde con 422 y 'Price must be a float between 1 and 999'", function (done) {
      chai
        .request(app)
        .post("/api/products")
        .set("Content-Type", "application/json")
        .send({ name: "producto 5", price: -1 })
        .end(function (err, res) {
          expect(res).to.have.status(422);

          const error = res.body.errors[0];

          expect(error.msg).to.be.equal(
            "Price must be a float between 1 and 999"
          );
          done();
        });
    });
    /**
     * Testea que no se crea documento por precio no valido
     */
    it("{name: 'producto 5', price: 1000} responde con 422 y 'Price must be a float between 1 and 999'", function (done) {
      chai
        .request(app)
        .post("/api/products")
        .set("Content-Type", "application/json")
        .send({ name: "producto 5", price: 1000 })
        .end(function (err, res) {
          expect(res).to.have.status(422);

          const error = res.body.errors[0];

          expect(error.msg).to.be.equal(
            "Price must be a float between 1 and 999"
          );
          done();
        });
    });
    /**
     * Testea que no se crea correctamente el documento
     */
    it("{name: 'producto 5', price: 500} responde con 400 y el documento creado", function (done) {
      const docToCreate = { name: "producto 5", price: 500 };
      chai
        .request(app)
        .post("/api/products")
        .set("Content-Type", "application/json")
        .send(docToCreate)
        .end(function (err, res) {
          expect(res).to.have.status(200);
          const createdDoc = res.body;

          // Elimina propiedad _id generada por MongoDB y __v
          // generada por Mongoose para realizar una comparación
          // correcta.
          delete createdDoc._id;
          delete createdDoc.__v;

          // Convierte ambos objetos en Strings y los compara
          expect(JSON.stringify(docToCreate, null, 2)).to.be.equal(
            JSON.stringify(createdDoc, null, 2)
          );

          done();
        });
    });
  });

  /**
   * Testea ProdcutController.removeProduct()
   */
  describe("DELETE /api/products/:id", function () {
    /**
     * Testea que /api/products/${id_existente} devuelva el documento
     * borrado.
     */
    it("/api/products/${idProd1} responde con 200 y producto 'product 1'.", (done) => {
      // Consultado un producto para obtener su id.
      ProductModel.find({ name: "product 1" }, function (err, docs) {
        const idToSearch = docs[0]._id;
        chai
          .request(app)
          .delete(`/api/products/${idToSearch}`)
          .end(function (err, res) {
            expect(res).to.have.status(200);
            expect(res.body._id).to.be.equal(idToSearch.toString());
            expect(res.body.name).to.be.equal("product 1");
            expect(res.body.price).to.be.equal(48.78);
            done();
          });
      });
    });
    /**
     * Testea que si se utiliza otra cosa que no sea un id de MongoDB
     * devuelva id invalido.
     */
    it("/api/products/loquesea responde con 400 e id invalido.", (done) => {
      chai
        .request(app)
        .delete(`/api/products/loquesea`)
        .end(function (err, res) {
          expect(res).to.have.status(400);
          const error = res.body.errors[0];
          expect(error.value).to.be.equal("loquesea");
          expect(error.msg).to.be.equal("ID not valid");
          done();
        });
    });
    /**
     * Testea que devuelva 404 si no encuentra el documento buscado.
     */
    it("/api/products/5fc23e76f77e69262cbfecea responde con 404.", (done) => {
      chai
        .request(app)
        .get(`/api/products/5fc23e76f77e69262cbfecea`)
        .end(function (err, res) {
          expect(res).to.have.status(404);
          expect(res.text).to.be.equal("Not Found");
          done();
        });
    });
  });

  /**
   * Testea ProdcutController.updateProduct()
   */
  describe("PUT /api/products", () => {
    /**
     * Testea que no se actualiza documento por nombre no válido
     */
    it("{id:${id_prod_2}, name:'product_2', price:22} responde con 422 y 'Name must contains only alphanumeric and whitespace charactes'", function (done) {
      ProductModel.find({ name: "product 2" }, function (err, docs) {
        const idToSearch = docs[0]._id;
        chai
          .request(app)
          .put(`/api/products/${idToSearch}`)
          .set("Content-Type", "application/json")
          .send({ name: "producto_2", price: 22 })
          .end(function (err, res) {
            expect(res).to.have.status(422);

            const error = res.body.errors[0];

            expect(error.msg).to.be.equal(
              "Name must contains only alphanumeric and whitespace charactes"
            );
            done();
          });
      });
    });
    /**
     * Testea que no se actualiza documento por nombre no válido
     */
    it("{id:${id_prod_2}, name:'prd2', price:22} responde con 422 y 'Name must contains only alphanumeric and whitespace charactes'", function (done) {
      ProductModel.find({ name: "product 2" }, function (err, docs) {
        const idToSearch = docs[0]._id;
        chai
          .request(app)
          .put(`/api/products/${idToSearch}`)
          .set("Content-Type", "application/json")
          .send({ name: "prd2", price: 22 })
          .end(function (err, res) {
            expect(res).to.have.status(422);

            const error = res.body.errors[0];

            expect(error.msg).to.be.equal(
              "Name must have a length between 5 and 50 characters"
            );
            done();
          });
      });
    });
    /**
     * Testea que no se actualiza documento por nombre no válido
     */
    it("{id:${id_prod_2}, name:'produuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuct', price:22} responde con 422 y 'Name must have a length between 5 and 50 characters'", function (done) {
      ProductModel.find({ name: "product 2" }, function (err, docs) {
        const idToSearch = docs[0]._id;
        chai
          .request(app)
          .put(`/api/products/${idToSearch}`)
          .set("Content-Type", "application/json")
          .send({
            name: "produuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuct",
            price: 22,
          })
          .end(function (err, res) {
            expect(res).to.have.status(422);

            const error = res.body.errors[0];

            expect(error.msg).to.be.equal(
              "Name must have a length between 5 and 50 characters"
            );
            done();
          });
      });
    });
    /**
     * Testea que se actualiza pasando solo una propiedad de un documento
     */
    it("{id:${id_prod_2}, name:'product 2'} responde con 200 y documento completo actualizado", function (done) {
      ProductModel.find({ name: "product 2" }, function (err, docs) {
        const idToSearch = docs[0]._id;
        chai
          .request(app)
          .put(`/api/products/${idToSearch}`)
          .set("Content-Type", "application/json")
          .send({ name: "prod 2" })
          .end(function (err, res) {
            expect(res).to.have.status(200);

            const docToUpdate = docs[0];
            // Sustituye valor name por valor usado en actualización
            // para compara documento anterior con el actualizado.
            docToUpdate.name = "prod 2";

            expect(JSON.stringify(docToUpdate, null, 2)).to.be.equal(
              JSON.stringify(res.body, null, 2)
            );
            done();
          });
      });
    });
    /**
     * Testea que se actualiza pasando solo una propiedad de un documento
     */
    it("{id:${id_prod_3}, price:222} responde con 200 y documento completo actualizado", function (done) {
      ProductModel.find({ name: "product 3" }, function (err, docs) {
        const idToSearch = docs[0]._id;
        chai
          .request(app)
          .put(`/api/products/${idToSearch}`)
          .set("Content-Type", "application/json")
          .send({ price: 222 })
          .end(function (err, res) {
            expect(res).to.have.status(200);

            const docToUpdate = docs[0];
            // Sustituye valor name por valor usado en actualización
            // para compara documento anterior con el actualizado.
            docToUpdate.price = 222;

            const expectedObj = JSON.stringify(docToUpdate, null, 2);
            const actualObj = JSON.stringify(res.body, null, 2);

            expect(expectedObj).to.be.equal(actualObj);
            done();
          });
      });
    });
  });
});
