"use strict";

require("dotenv").config(); // Carga variables de entorno
const express = require("express"); // Framework par Node.js
const morgan = require("morgan"); // Logger estilo Apache Web Server
const logger = require("./Config/logger"); // Logger para resto operaciones
const mongoose = require("mongoose"); // ORM para MongoDB
const Db = require("./Config/initDB"); // Inicia conexión con BBDD
const helmet = require("helmet");

/* DB */
Db.connect();
function intervalFunc() {
  // console.log(mongoose.connection.readyState);
  if (mongoose.connection.readyState == 0) {
    // console.log("Trying to reconnect to DB");
    logger.error("Trying to reconnect to DB");
    Db.connect();
  }
}
// Ejecuta la función anterior cada x segundos (1000 milisegundos)
const secondToCheck = process.env.DB_HEALTH_CHECK_RATE;
// Node.js: Timers
if (process.env.NODE_ENV !== "test")
  setInterval(intervalFunc, secondToCheck * 1000);
/* End DB */

// Crea una instancia de Express.
const app = express();

/////

// const { RateLimiterMemory } = require('rate-limiter-flexible');
// const opts = {
//   points: 6, // 6 points
//   duration: 1, // Per second
// };

// const rateLimiter = new RateLimiterMemory(opts);

// rateLimiter.consume(remoteAddress, 2) // consume 2 points
//     .then((rateLimiterRes) => {
//       // 2 points consumed
//     })
//     .catch((rateLimiterRes) => {
//       // Not enough points to consume
//     });

/////

/* Middlewares */

// Parsea body peticiones HTTP com objetos
app.use(express.json());

// Registra acceso todas peticiones
app.use(morgan("combined", { stream: logger.stream }));

// Securiza express de algunas vulnerabilidades bien conocidas
// modifcando las cabeceras HTTP
app.use(helmet());

// Si se pierde conexión con BBDD devuelve 500 al acceder a
// cualquier ruta diferente de /api/health
app.use(function (req, res, next) {
  if (req.url.toString() === "/api/health") next();
  else if (mongoose.connection.readyState == 1) next();
  else {
    // console.log("DB NOT connected");
    logger.error("DB not connected");
    res.sendStatus(500);
  }
});

/* End middlewares */

//don't show the log when it is test
// if (process.env.NODE_ENV !== "test") {
//   //use morgan to log at command line
//   app.use(morgan("combined")); //'combined' outputs the Apache style LOGs
// }

// Check if server is up
app.use("/api/health", require("./Routes/healthRoutes"));
// Rutas API productos
app.use("/api/products", require("./Routes/ProductsRoutes"));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.sendStatus(404);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.sendStatus(err.status || 500).end();
});

const PORT = process.env.PORT || 5000;
module.exports = app.listen(PORT, () =>
  logger.info(`Server running on port ${PORT}`)
);
