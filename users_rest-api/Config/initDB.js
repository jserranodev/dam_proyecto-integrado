"use strict";

const mongoose = require("mongoose");
require("dotenv").config();
const logger = require("./logger");

module.exports = {
  connect() {
    mongoose
      .connect(process.env.MONGODB_URI, {
        dbName: process.env.DB_NAME,
        useFindAndModify: false, //https://mongoosejs.com/docs/deprecations.html#findandmodify
        useUnifiedTopology: true, // Usa MongoDB driver 3.3.x
        useNewUrlParser: true, //s habilita nuevo parser
        useCreateIndex: true, // Elimina warning 'collection.ensureIndex is deprecated'
      })
      .then(() => {
        // console.log("DB Connected!");
        logger.info("BD connected");
      })
      // Se ejecuta trás él último intento de conexión
      .catch((err) => {
        logger.error(`DB Connection Error: ${err}`);
        // console.log(`DB Connection Error: ${err.message}`);
        // // Vuelve a ejecutar este método hasta que se establezca
        // // conexión.
        // this.connect();
      });
  },
  getConnection() {
    this.currConn = mongoose.connection;
  },

  // setEvents() {
  //   this.getConnection;
  //   this.currConn.on("connecting", console.log("Connecting..."));
  //   this.currConn.on("connected", console.log("Connected"));
  //   this.currConn.on("disconnected", console.log("Disconnected"));
  //   this.currConn.on("reconnectFailed", () => {
  //     console.log("Run out of reconectTries");
  //     this.connect();
  //   });
  // },
};
