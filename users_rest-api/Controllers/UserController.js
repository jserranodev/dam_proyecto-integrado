"use strict";

const { param, body, validationResult } = require("express-validator");
const logger = require("../Config/logger");
const UserModel = require("../Models/UserModel");
const createError = require("http-errors");

module.exports = {
  /**
   * Devuelve conjunto validadores en función nombre método a validar
   * @param {String} method Nombre método a validar
   */
  validate(method) {
    switch (method) {
      case "create": {
        return [
          body("username")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("username is required")
            .normalizeEmail()
            .isEmail()
            .withMessage("username must be a valid email"),
          body("password")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("password is required")
            .isAlphanumeric()
            .withMessage("password must only contains alphanumeric characters")
            .isLength({ min: 10, max: 24 })
            .withMessage(
              "password must have a length between 10 and 24 characters"
            )
            .custom((value, { req }) => {
              if (value !== req.body.confirmPassword) {
                // trow error if passwords do not match
                throw new Error(
                  "Password and confirmation password don't match"
                );
              } else {
                return value;
              }
            }),
        ];
      }
      case "authenticate": {
        return [
          body("username")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("username is required")
            .normalizeEmail()
            .isEmail()
            .withMessage("username must be a valid email"),
          body("password")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("password is required"),
        ];
      }
      case "updateNoAuthData": {
        return [
          param("userId")
            .exists({ checkNull: true, checkFalsy: true })
            .isMongoId(),
          body("firstname")
            .exists({ checkNull: true, checkFalsy: true })
            .isAlpha(),
          body("lastname")
            .exists({ checkNull: true, checkFalsy: true }) // Comprobación
            .isAlpha(),
          body("address.address_line")
            .exists({ checkNull: true, checkFalsy: true })
            .notEmpty()
            .isArray(),
          // (.*) Selecciona elementos array / objeto
          body("address.address_line.*").custom((name) => {
            // regex: letras, números y espacios en blanco
            const result = /^[a-zA-Z0-9 ÑñÁáÉéÍíÓóÚúÜü]*$/.test(name);
            if (result) return Promise.resolve();
            return Promise.reject("address.address_line contains invalid data");
          }),
          body("address.admin_area")
            .exists({ checkNull: true, checkFalsy: true })
            .notEmpty()
            .isArray(),
          body("address.admin_area.*").custom((name) => {
            // regex: letras, números y espacios en blanco
            const result = /^[a-zA-Z0-9 ÑñÁáÉéÍíÓóÚúÜü]*$/.test(name);
            if (result) return Promise.resolve();
            return Promise.reject("address.admin_area contains invalid data");
          }),
          body("address.postal_code")
            .exists({ checkNull: true, checkFalsy: true })
            .isInt({ min: 0 }),
          body("address.country_code")
            .exists({ checkNull: true, checkFalsy: true })
            .isUppercase()
            .isISO31661Alpha2(), // comprueba si es código de país de 2 letras
        ];
      }
      case "updatePassword": {
        return [
          param("userId")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("ID is required")
            .isMongoId()
            .withMessage("ID not valid"),
          body("oldPassword")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("old password is required")
            .isAlphanumeric()
            .withMessage(
              "old password must only contains alphanumeric characters"
            ),
          body("newPassword")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("new password is required")
            .isAlphanumeric()
            .withMessage(
              "new password must only contains alphanumeric characters"
            ),
        ];
      }
      case "deleteUserByUsername": {
        return [
          body("username")
            .exists({ checkNull: true, checkFalsy: true })
            .withMessage("username is required")
            .normalizeEmail()
            .isEmail()
            .withMessage("username must be a valid email"),
        ];
      }
    }
  },
  /**
   * Crea un usuario
   */
  async create(req, res, next) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        logger.info(
          "POST /api/users/create UsersController.create()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        return res.status(422).json({ errors: errors });
      }
      // Extrae cuerpo de petición
      const { username, password } = req.body;

      const findedUser = await UserModel.findOne({ username: username });

      // Si user NO es truthy responde con error
      if (findedUser) {
        // Imprescindible return, sin el termina de ejecutar el
        // código, crea el usuario y luego devuelve respuesta de
        // 'user already exist'
        return res.status(400).json({
          errors: [
            {
              msg: "user already exists",
              param: "username",
              location: "body",
            },
          ],
        });
      }

      const createdUser = await UserModel.create({
        username: username,
        password: password,
      });
      if (createdUser) {
        return res.status(201).json(createdUser);
      } else {
        logger.error(
          "POST /api/users UsersController.create()" + err
        );
        next(createError(500));
      }
      //
    } catch (err) {
      logger.error(
        "POST /api/users UsersController.create()" + err
      );
      next(err);
    }
  },
  /**
   * Autentica a un usuario usando su username y contraseña
   */
  async authenticate(req, res, next) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        logger.info(
          "POST /api/users/auth UsersController.authenticate()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(422).json({ errors: errors });
      }
      // Extrae cuerpo de petición
      const { username, password } = req.body;

      const user = await UserModel.findOne({ username: username });

      // Si user NO es truthy lazan error
      if (!user) {
        res
          .status(404)
          // .status(200)
          .json({
            errors: [
              { msg: "user not found", param: "username", location: "body" },
            ],
          });
      }

      // validPass: Boolean
      const validPass = await user.comparePassword(password);
      if (validPass) {
        res.status(200).json(user);
      } else {
        res.status(401).json({
          errors: [
            { msg: "invalid password", param: "password", location: "body" },
          ],
        });
      }
      //
    } catch (err) {
      next(err);
    }
  },
  /**
   * Actualiza datos no utilizados en la autenticación
   */
  async updateNonAuthData(req, res, next) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });
      if (errors.length !== 0) {
        logger.info(
          "PATCH /api/users/:id/non-auth-data UsersController.updateNonAuthData() " +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(422).json({ errors: errors });
      }
      // res.json({ msg: "no auth data validated!" });

      // Usa id usuario como filtro
      const filter = { _id: req.params.userId };
      // Crea objeto copia de cuerpo petición spara actualizar
      const update = req.body;
      // Busca y actualiza
      const updatedUser = await UserModel.findOneAndUpdate(filter, update, {
        returnOriginal: false,
      });
      // Si updateUser es truthy devuelve objeto
      // Si es falsy no se ha encontrado usuario
      if (updatedUser) res.status(200).json(updatedUser);
      next(createError(404));
      //
    } catch (error) {
      next(createError(500));
    }
  },
  /**
   * Actualiza contraseña un usuario
   */
  async updatePassword(req, res, next) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });
      if (errors.length !== 0) {
        logger.info(
          "PUT /api/users UsersController.update() " +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        res.status(422).json({ errors: errors });
      }
      // const productToUpdate = product;

      // productToUpdate.name = req.body.name || product.name;
      // productToUpdate.price = fixedPrice || product.price;
      // productToUpdate.save().then((updatedProduct) => {
      //   if (updatedProduct) res.json(updatedProduct);
      //   else res.sendStatus(500);

      const userId = req.params.userId;
      const { oldPassword, newPassword } = req.body;
      const userToUpdate = await UserModel.findOne({ _id: userId });

      // Si no es ha encontrado usuario responde con 404: recurso no
      // encontrado
      if (!userToUpdate)
        res.status(404).json({
          errors: [
            { msg: "user not found", param: "username", locatin: "body" },
          ],
        });

      // Si usuario encontrado pero contraseña vieja no coincide con
      // contraseña en bbdd responde con 400: bad request
      if (!(await userToUpdate.comparePassword(oldPassword)))
        res.status(400).json({
          errors: [
            {
              msg: "invalid password",
              param: "oldPassword",
              locatin: "body",
            },
          ],
        });
      userToUpdate.password = newPassword;
      const updatedUser = await userToUpdate.save();
      res.status(200).json(updatedUser);
    } catch (error) {
      logger.error("PUT /api/users UsersController.updatePassword() " + error);
      next(err);
    }
  },
  /**
   * Elimina usuario con ID igual al pasado por parámetro
   */
  async deleteUserByUsername(req, res, next) {
    try {
      const errors = validationResult(req).array({ onlyFirstError: true });

      if (errors.length !== 0) {
        logger.info(
          "DELETE /api/users/:userId UsersController.deleteUser()" +
            errors.map((x) => JSON.stringify(x, null, 2))
        );
        return res.status(422).json({ errors: errors });
      }
      // Extrae cuerpo de petición
      const { username } = req.body;

      const user = await UserModel.findOne({ username: username });

      // Si user NO es truthy lazan error
      if (!user) {
        return (
          res
            .status(404)
            // .status(200)
            .json({
              errors: [
                { msg: "user not found", param: "username", location: "body" },
              ],
            })
        );
      }

      // validPass: Boolean
      const deletedUser = await UserModel.findOneAndDelete({
        username: username,
      });
      if (deletedUser) {
        return res.status(200).json(deletedUser);
      } else {
        next(createError(500));
      }
      //
    } catch (err) {
      next(err);
    }
  },
};
