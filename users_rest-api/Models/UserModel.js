"use strict";

/* Imports */
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt"); // función de hasheo para contraseñas

/* Varibales */
const SALT_WORK_FACTOR = 10;

const AddressSchema = new Schema(
  {
    // calle, número, planta, ...
    address_line: { type: [String] },
    // ciudad, localidad, estado, comunidad autonoma, ...
    admin_area: { type: [String] },
    // Código postal
    postal_code: { type: Number },
    // Código de país
    country_code: { type: String, match: /[A-Z]{2}/ }, // US, ES
  },
  {
    // Deshabilita campo _id en subdocumento
    _id: false,
  }
);

const UserSchema = new Schema(
  {
    // nombre usuario
    username: { type: String, index: true, unique: true, required: true },
    // Contraseña
    password: { type: String, required: true },
    // Nombre
    firstname: { type: String },
    // Apellido
    lastname: { type: String },
    // Dirección
    address: { type: AddressSchema },
  },
  {
    // Habilita cración campos createdAt y updatedAt
    timestamps: true,
  }
);

// UserSchema.pre("save", function (next) {
UserSchema.pre("save", function (next) {
  const user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified("password")) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});

/**
 * Convierte a hash la contraseña de entrada y la compara con la que hay en la bbdd
 * @param {string} candidatePassword contraseña a comprobar
 *
 */
UserSchema.methods.comparePassword = function (candidatePassword) {
  return bcrypt
    .compare(candidatePassword, this.password)
    .then((isMatch) => isMatch)
    .catch((err) => err);
};

const UserModel = mongoose.model("user", UserSchema);

module.exports = UserModel;
