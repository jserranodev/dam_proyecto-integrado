var express = require("express");
var router = express.Router();

const UserController = require("../Controllers/UserController");

router
  /**
   * POST /api/users Crea un usuario
   */
  .post("", UserController.validate("create"), UserController.create)
  /**
   * POST /api/users/auth Comprueba si usuario y contraseña introducidos
   * coinciden con datos bbdd. Si coincide autentica al usuario
   */
  .post(
    "/auth",
    UserController.validate("authenticate"),
    UserController.authenticate
  )
  /**
   * PATCH /api/users/:userId/non-auth-data Actualiza información de
   * usuario no utilizada para autenticación
   */
  .patch(
    "/:userId/non-auth-data",
    UserController.validate("updateNoAuthData"),
    UserController.updateNonAuthData
  )
  /**
   * PATCH /api/users/:userId/password Actualiza contraseña
   */
  .patch(
    "/:userId/password",
    UserController.validate("updatePassword"),
    UserController.updatePassword
  )
  /**
   * DELETE /api/users/:username
   */
  .delete('/by-username',
  UserController.validate('deleteUserByUsername'),
  UserController.deleteUserByUsername)

module.exports = router;
