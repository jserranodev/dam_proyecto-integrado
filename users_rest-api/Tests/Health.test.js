"use strict";

const chai = require("chai"),
  chaiHttp = require("chai-http");
const mongoose = require("mongoose");
const expect = chai.expect;
chai.use(chaiHttp);

/**
 * Funciones que testean end-point /api/healt
 */
describe("Test salubridad servidor", function () {
  let app = undefined;

  /**
   * Inicia el servidor antes de realizar los tests.
   */
  before((done) => {
    app = require("../app");
    done();
  });

  /**
   * Trás finalizar cierra el servidor y la conexión con la BBDD que
   * se inicia al arrancar el servidor.
   */
  after((done) => {
      app.close(() => done());
  });

  /**
   * Testea /api/health
   */
  describe("GET /api/health", () => {
    /**
     * Testea que /api/productos devuelva todos los documentos
     */
    it("/api/health responde con 200", function (done) {
      chai
        .request(app)
        .get("/api/health")
        .end(function (err, res) {
          // console.log(res.body);
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
