"use strict";

const mongoose = require("mongoose");
// const request = require("supertest");
const chai = require("chai"),
  chaiHttp = require("chai-http");
const expect = chai.expect;
chai.use(chaiHttp);

// const UserModel = require("../Models/UserModel");

/**
 * Conjunto de funciones que testean el el router, controlador y
 * modelo de productos.
 */
describe("Test products", function () {
  let app = undefined;
  let createdUser = undefined;

  /**
   * Inicia el servidor antes realizar tests
   */
  before((done) => {
    app = require("../app");
    done();
    // ProductModel.insertMany(mock_data, function (error, docs) {
    //   done();
    // });
  });
  after((done) => {
    mongoose.connection.db.dropCollection("users").then(() => {
      mongoose.connection.close(() => {
        app.close(() => done());
      });
    });
  });

  /**
   * Testea UserController.create()
   */
  describe("POST /api/users", () => {
    /**
     * Testea que /api/users devuelva el usuario creado
     */
    it("Responde con 201 y el usuario creado", function (done) {
      const userToCreate = {
        username: "user@one.es",
        password: "passwordpassword",
        confirmPassword: "passwordpassword",
      };
      chai
        .request(app)
        .post("/api/users")
        .set("Content-Type", "application/json")
        .send(userToCreate)
        .end(function (err, res) {
          // console.log(res.body);
          expect(res).to.have.status(201);

          createdUser = res.body;

          // Comprueba objeto devuelto tenga propriedades correctas
          expect(createdUser).to.have.keys([
            "_id",
            "username",
            "password",
            "createdAt",
            "updatedAt",
            "__v",
          ]);
          done();
        });
    });
  });
  /**
   * Testea UserController.authenticate()
   */
  describe("POST /api/users/auth", () => {
    /**
     * Testea que /api/users devuelva el usuario creado
     */
    it("Responde con 200 y el usuario logueado", function (done) {
      const userToAuth = {
        username: "user@one.es",
        password: "passwordpassword",
      };
      chai
        .request(app)
        .post("/api/users/auth")
        .set("Content-Type", "application/json")
        .send(userToAuth)
        .end(function (err, res) {
          console.log(res.body);
          expect(res).to.have.status(200);

          const authenticateUser = res.body;

          // Convierte objetos en string para poder ser comparados
          const authUserToString = JSON.stringify(authenticateUser, null, 2);
          const createdUserToString = JSON.stringify(createdUser, null, 2);

          expect(authUserToString).to.be.eq(createdUserToString);

          done();
        });
    });
  });
});
