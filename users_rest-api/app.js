const express = require("express");
const cookieParser = require("cookie-parser");
// const logger = require("morgan");
const morgan = require("morgan"); // Logger estilo Apache Web Server
const logger = require("./Config/logger"); // Logger para resto operaciones
const bodyParser = require("body-parser");
const createError = require("http-errors");
const Db = require("./Config/initDB");
const app = express();
const mongoose = require("mongoose");
require("dotenv").config();

/* DB */
Db.connect();
function intervalFunc() {
  // console.log(mongoose.connection.readyState);
  if (mongoose.connection.readyState == 0) {
    // console.log("Trying to reconnect to DB");
    logger.error("Trying to reconnect to DB");
    Db.connect();
  }
}
// Ejecuta la función anterior cada 5 segundo (5000 milisegundos)
const secondToCheck = process.env.DB_HEALTH_CHECK_RATE;
// Node.js: Timers
if (process.env.NODE_ENV !== "test")
  setInterval(intervalFunc, secondToCheck * 1000);
/* End DB */

/* Express middlewares */
app.use(morgan("combined", { stream: logger.stream }));
app.disable("x-powered-by");
app.use(bodyParser.json());
// app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Check if server is up
app.use("/api/health", require("./Routes/HealthRoutes"));
app.use("/api/users", require("./Routes/UserRoutes"));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// errorHandler
app.use(function (err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  }
  // usa código estado de error o 500
  res.status(err.status || 500).json({ error: err });
});

const PORT = process.env.PORT || 5000;
module.exports = app.listen(PORT, () =>
  logger.info(`Server running on port ${PORT}`)
  // console.info(`Server running on port ${PORT}`)
);
