"use strict";

const { createLogger, format, transports } = require("winston");
require('dotenv').config()
const NODE_ENV = process.env.NODE_ENV
//
// Si propiedad handleException toma valor true durante los tests,
// Mocha no mostrará errores porque los captura Winston.
//
const handleExceptionValue = /^(dev|test)$/.test(NODE_ENV) ? false : true

const logger = createLogger({
  level: "info",
  format: format.combine(
    format.timestamp({
      format: "YYYY-MM-DD HH:mm:ss",
    }),
    format.errors({ stack: true }),
    format.splat(),
    format.json()
  ),
  defaultMeta: { service: "products_res-api" },
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`.
    // - Write all logs error (and below) to `error.log`.
    //
    new transports.File({
      filename: "./Logs/error.log",
      level: "error",
      handleExceptions: handleExceptionValue,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: false,
    }),
    new transports.File({
      filename: "./Logs/combined.log",
      handleExceptions: handleExceptionValue,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: false,
    }),
  ],
});

//
// Si estamos en desarrollo (dev) o produccion (prod) escribe en
// consola.
// Si estamos en test NO escribe en consola.
//
if ( NODE_ENV !== "test") {
  logger.add(
    new transports.Console({
      format: format.combine(format.colorize(), format.simple()),
    })
  );
}

logger.stream = {
  write: function (message, encoding) {
    // use the 'info' log level so the output will be picked up by both transports (file and console)
    logger.info(message);
  },
};

module.exports = logger;
