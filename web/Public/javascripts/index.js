"use strict";

// Redirige a página principal al pulsar sobre cabecera
const header = document.getElementById("header");
header.addEventListener("click", async function () {
  try {
    document.location.href = "/products";
    // fetch('/orders/capture-paypal-transaction').then(res=>res.json()).then(data=>console.log(data))

    // const response = await fetch("/orders/capture-paypal-transaction");
    // const msg = await response.json();
    // console.log(msg);
  } catch (error) {
    console.log(error);
  }
});

/* Dashboard */
// Cambia entre vista orders y profile
const btnOrders = document.getElementById("btn-orders");
if (btnOrders) {
  btnOrders.addEventListener("click", (event) => {
    document.getElementById("profile").classList.add("d-none");
    document.getElementById("orders").classList.remove("d-none");
  });
}
const btnProfile = document.getElementById("btn-profile");
if (btnProfile) {
  btnProfile.addEventListener("click", (event) => {
    document.getElementById("orders").classList.add("d-none");
    document.getElementById("profile").classList.remove("d-none");
  });
}
// Elimina botón refund si pedido tiene estado refunded
const ordersTable = document.getElementById("orders-table");
if (ordersTable) {
  for (let i = 1; i < ordersTable.rows.length; i++) {
    ordersTable.rows[i].cells["status-col"].textContent === "refunded"
      ? ordersTable.rows[i].cells["options-col"]
          .querySelector("#btn-refund")
          .remove()
      : console.log("hola");
  }
}
/* End dashboard */

