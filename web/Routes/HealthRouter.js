"use strict";

const express = require("express");
const router = express.Router();
const logger = require("../Config/Logger");

// Comprueba si servidor funciona
router.get("/", (req, res) => {
  logger.info("Checked server health");
  res.sendStatus(200);
});

module.exports = router;