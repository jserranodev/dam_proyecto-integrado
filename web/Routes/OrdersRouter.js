"use strict";

const express = require("express");
const router = express.Router();
const axios = require("axios").default;
const createError = require("http-errors");
const logger = require('logger')
require("dotenv");

const instance = axios.create({
  baseURL: process.env.ORDERS_REST_API_URI,
  // Necesita más tiempo. Rest-API hace petición a Paypal. Puede
  // tomar más de 1 segundo. Si no se da algo más de tiempo Axios
  // corta conexión y falla petición
  timeout: 1000 * 5,
  // headers: {'X-Custom-Header': 'foobar'}
});

router
  /**
   * POST /orders/capture-paypal-transaction Captura información
   * transacción Paypal realizada y almacena datos en bbdd.
   */
  .post("/capture-paypal-transaction", async (req, res, next) => {
    // console.log(`\nCapturada transacción: ${req.body}\n`);
    // res.status(200).json({payer_given_name: 'Server-Antonio'});
    try {
      // Datos a enviar a rest api
      const userId = req.session.user._id;
      const data = {
        userId: userId,
        orderID: req.body.orderID,
        prodId: req.body.prodId,
      };

      const response = await instance.post("", data);

      // Si operación correcta devuelve 201: Created
      res.status(201).end();
    } catch (error) {
      // console.log(error);
      logger.error(
        "POST /orders/capture-paypal-transaction " + err
      );
      next(error);
    }
  })
  /**
   * POST /orders/refund-paypal-transaction Realiza devolución de un
   * pedido pagado con Paypal.
   */
  .post("/refund-paypal-transaction", async (req, res, next) => {
    try {
      // Recive y envía a rest-api ID del pago
      const orderId = req.body.orderId;
      const data = { orderId: orderId };
      const response = await instance.patch("/refund", data);
      // Si no hay error renderiza página personal
      res.redirect("/users/dashboard");
    } catch (error) {
      logger.error(
        "POST /orders/capture-paypal-transaction " + err
      );
      next(createError(500));
    }
  });

module.exports = router;
