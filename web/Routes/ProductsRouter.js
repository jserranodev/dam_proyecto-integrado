"use strict";

const express = require("express");
const router = express.Router();
const axios = require("axios").default;
const { body, validationResult } = require("express-validator");
require("dotenv");

const instance = axios.create({
  baseURL: process.env.PRODUCTS_REST_API_URI,
  timeout: 1000 * 2,
  // headers: {'X-Custom-Header': 'foobar'}
});

router
  /**
   * GET /products Renderiza productos forma páginada
   */
  .get("", (req, res, next) => {
    const data = { page: req.query.page || 1, limit: 6 };
    instance
      .get("/", {
        data: data,
      })
      .then(function (axiosRes) {
        // res.json(axiosRes.data);
        // console.log(axiosRes.data);
        res.render("index", { products: axiosRes.data });
      })
      .catch(function (err) {
        // console.log(err.data);
        next(err);
      });
  })
  /**
   * GET /products/:id Renderiza página detalles producto con ID
   * igual a parámetro :id
   */
  .get("/:id", async (req, res, next) => {
    try {
      const productId = req.params.id;
      const response = await instance.get(`/${productId}`);

      // Almacena id producto por si usuario inicia sesión desde
      // página producto poder redireccionarlo de vuelta a esta.
      req.session.last_product_seen = productId;

      // Pasa a motor render usernama para saber si esta logueado y
      // producto para mostrar sus datos.
      res.render("product-details", {
        product: response.data,
        paypalClientId: process.env.PAYPAL_CLIENT_ID,
      });
    } catch (error) {
      // Si error tiene response es un error controlado con errores de validación.
      // Muestra errores obtenidos
      if (error.response) {
        const { status, data } = error.response;
        res.status(status).render("login", { errors: data.errors });
        // Si error no tiene propiedad response ocurrió otro error
      } else {
        res.status(500).render("error-user-friendly");
      }
    }
  })
  /**
   * POST /products/search Busca y renderiza lista productos que
   * contengan en nombre termino búscado por usuario.
   */
  .post(
    "/search",
    [
      // Validación termino búscado
      body("name")
        .trim()
        .exists({ checkNull: true, checkFalsy: true })
        .withMessage("Invalid product name")
        .custom((name) => {
          // regex: letras, números y espacios en blanco
          const result = /^[a-zA-Z0-9 ]*$/g.test(name);
          if (result) return Promise.resolve();
          return Promise.reject("Invalid product name");
        }),
    ],
    async (req, res, next) => {
      try {
        // Si hay errores en validación retorna 400 y array errores
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(400).render("search", {
            errors: errors.array({ onlyFirstError: true }),
          });
        }

        const { name } = req.body;
        // console.log(name);
        const response = await instance.get(`/search-by-name/${name}`, {
          data: { name },
        });
        res.status(200).render("search", { products: response.data });
      } catch (error) {
        // Si no se a encontrado o se han envíado mal los datos
        // renderiza página de búsqueda vacía
        if (error.response.status === 404 || error.response.status === 422) {
          res.status(200).render("search", { products: null });
        } else next(error);
      }
    }
  );
// .post("/", (req, res, next) => {
//   instance
//     .post("/", req.body)
//     .then(function (axiosRes) {
//       res.json(axiosRes.data);
//     })
//     .catch(function (err) {
//       next(err);
//     });
// })
// .put("/:productId", (req, res, next) => {
//   instance
//     .put(req.params.productId, req.body)
//     .then(function (axiosRes) {
//       res.json(axiosRes.data);
//     })
//     .catch(function (err) {
//       next(err);
//     });
// })
// .delete("/:productId", (req, res, next) => {
//   instance
//     .delete(req.params.productId)
//     .then(function (axiosRes) {
//       res.json(axiosRes.data);
//     })
//     .catch(function (err) {
//       next(err);
//     });
// });

module.exports = router;
