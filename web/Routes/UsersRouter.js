const express = require("express");
const router = express.Router();
const createError = require("http-errors");
const axios = require("axios").default;
const { body, validationResult } = require("express-validator");
const logger = require("../Config/Logger");
require("dotenv");

const instance = axios.create({
  baseURL: process.env.USERS_REST_API_URI,
  timeout: 1000 * 1,
  // headers: {'X-Custom-Header': 'foobar'}
});

router
  /**
   * GET /users/login Renderiza formulario inicia de sessión
   */
  .get("/login", (req, res) => {
    res.render("login");
  })
  /**
   * GET /users/dashboard Renderiza el espacio personal del usuario logueado
   */
  .get("/dashboard", async (req, res, next) => {
    // Aislar uso 'request' diferente contextos para evitar fallos
    // establecimiento.
    //
    // Si usuario logueado accede a dashboard
    // Si no logueado redirecciona a /login
    if (req.session.user) {
      try {
        const orders_rest_api_uri = process.env.ORDERS_REST_API_URI;
        const response = await instance.get(
          `${orders_rest_api_uri}/userid/${req.session.user._id}`
        );
        //
        // Recorre fechas creación y las pasa a formato europeo
        for (let i = 0; i < response.data.length; i++) {
          const order = response.data[i];
          // Ej. fecha bbdd: 2020-12-14T12:06:49.019Z
          const createdAt = order.createdAt.split("T")[0];
          const splitDate = createdAt.split("-");
          const eurDateFormat = `${splitDate[2]}/${splitDate[1]}/${splitDate[0]}`;
          console.log(eurDateFormat);
          response.data[i].createdAt = eurDateFormat;
        }
        //
        res.status(200).render("dashboard", { orders: response.data });
      } catch (error) {
        logger.error(
          "GET /users/dashboard UsersRouter" + error
        );
        if (error.status === 404) {
          logg
          res.status(404).render("dashboard");
        } else {
          if(process.env.NODE_ENV === 'prod'){res.status(500).render('error-user-friendly')}
          else {next(createError(500))}
        }
      }
    } else {
      res.redirect("login");
    }
  })
  /**
   * GET /users/logout elimina variable sesión y redirecciona a página de inicio
   */
  .get("/logout", (req, res) => {
    req.session.destroy();
    res.redirect("/products");
  })
  /**
   * GET /users/signup Renderiza formulario de alta
   */
  .get("/signup", (req, res) => {
    res.render("signup");
  })
  /**
   * POST /users/login Inicia sesión de un usuario
   */
  .post(
    "/login",
    // Array de validaciones
    [
      body("email")
        .exists({ checkNull: true, checkFalsy: true })
        // Si no cumple validación anterior retorna este mensaje de error
        .withMessage("Username required")
        .normalizeEmail()
        .isEmail()
        .withMessage("Username must be a valid email"),
      body("password")
        .exists({ checkNull: true, checkFalsy: true })
        .withMessage("password is required")
        .isLength({ min: 5, max: 50 })
        .withMessage("Password length invalid"),
    ],
    async (req, res, next) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          res.status(400).render("login", {
            errors: errors.array({ onlyFirstError: true }),
          });
        }

        // Extrae parámetros de petición, crea objeto usuario y hace
        // petición de creación en bbdd
        const user = { username: req.body.email, password: req.body.password };
        const response = await instance.post("/auth", user);

        // Carga datos usuario en variable sesión para acceder a ellos
        // sin consultar bbdd
        req.session.user = response.data;

        // Si usuario acceder login desde página producto, lo devuelve a llí.
        if (req.session.last_product_seen) {
          res.redirect(`/products/${req.session.last_product_seen}`);
        } else {
          // res.redirect("/users/dashboard");
          res.redirect("/users/dashboard");
        }
        //
      } catch (error) {
        logger.error(
          "POST /users/login UsersRouter" + error
        );
        // Si error tiene propiedad response hubo problema al pasar o validar datos
        if (error.response) {
          const { status, data } = error.response;
          res.status(status).render("login", { errors: data.errors });
          // Si error no tiene propiedad response ocurrió otro error
        } else {
          res.status(500).render("error-user-friendly");
        }
      }
    }
  )
  /**
   * POST /users/sigunp Da de alta a un usuario
   */
  .post(
    "/signup",
    [ // Array de validaciones
      body("email")
        .exists({ checkNull: true, checkFalsy: true })
        .withMessage("Username required")
        .normalizeEmail()
        .isEmail()
        .withMessage("Username must be a valid email"),
      body("password")
        .exists({ checkNull: true, checkFalsy: true })
        .withMessage("password is required")
        .isLength({ min: 5, max: 50 })
        .withMessage("Password length invalid")
        .custom((value, { req }) => {
          if (value !== req.body.confirmPassword) {
            // trow error if passwords do not match
            throw new Error("Password and confirmation password don't match");
          } else {
            return value;
          }
        }),
    ],
    async (req, res, next) => {
      try {
        // Datos a enviar a rest api
        const postData = {
          username: req.body.email,
          password: req.body.password,
          confirmPassword: req.body.confirmPassword,
        };
        // console.log(postData);
        const response = await instance.post("", postData);
        // Guarda datos usuario devueltos por petición en variable de sessión
        req.session.user = response.data;
        // Renderiza zona personal
        res.status(200).render("dashboard");
        // res.redirect('/dashboard')
      } catch (error) {
        logger.error(
          "POST /users/signup UsersRouter" + error
        );
        console.log(error);
        if (error.response) {
          const { status, data } = error.response;
          res
            .status(status)
            .render("signup", { errors: data.errors, email: req.body.email });
        } else {
          // next(error);
          res.status(500).render("error-user-friendly");
        }
      }
    }
  );

module.exports = router;
