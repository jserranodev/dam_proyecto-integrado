"use strict";

require("dotenv").config;
const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
// const logger = require("morgan");
const morgan = require("morgan"); // Logger estilo Apache Web Server
const logger = require("./Config/Logger"); // Logger para resto operaciones
const helmet = require("helmet");
// const { data } = require("./Config/Logger");
const session = require("express-session");
const uid = require("uid-safe");

const app = express();

/* view engine setup */
app.set("views", path.join(__dirname, "Views"));
// const hbs = require("hbs");
// hbs.registerHelper("isRefunded", function (value) {
//   return value === "refunded";
// });
// app.set("view engine", "pug")
app.set("view engine", "hbs");

/* Middleware stack */
// app.use(logger("dev"));
app.use(morgan("combined", { stream: logger.stream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// Habilita uso de sesiones
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      // miliseconds * seconds * minutes * hours
      maxAge: 1000 * 60 * 60 * 1,
    },
    genid: uid(18, (err, string, next) => (err ? next(err) : string)),
    rolling: true,
  })
);
// Permite acceder session desde view engine
app.use(function (req, res, next) {
  res.locals.session = req.session;
  next();
});
app.use(cookieParser());
// Desahbilita cabecera respuestas comunica que tipo de servidor es
app.disable("x-powered-by");
// app.use(helmet()); // BLOQUEA SCRIPT PAYPAL. Necesita añadirse nonce o SHA al script y csp
// Establece directorio contenido estático
app.use(express.static(path.join(__dirname, "Public")));
// Establece bootstrap en directorio estático para estilos
app.use(
  "/css",
  express.static(path.join(__dirname, "node_modules/bootstrap/dist/css"))
);
// Rutas
app.use("/api/health", require("./Routes/HealthRouter"));
app.use("/users", require("./Routes/UsersRouter"));
app.use("/orders", require("./Routes/OrdersRouter"));
app.use("/products", require("./Routes/ProductsRouter"));
// Redirecciona / a /products
app.use("", (req, res, next) => res.redirect("/products"));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  // next(createError(404));
  res.render("not-found");
});

// error handler
app.use(function (err, req, res, next) {
  let statusCode, view, returnData;
  if (res.headersSent) {
    return next(err);
  }
  res.status(500);
  res.render("error", { error: err });
});
/* End middleware stack */

// console.log(process.env.USERS_REST_API_URI);

module.exports = app;
